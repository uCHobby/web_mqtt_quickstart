
function main(){
	var buttonElement = document.getElementById('dateButton');
	buttonElement.addEventListener("click",onButtonClick);
}

function onButtonClick(event){
	var dateLabelElement = 	document.getElementById('dateLabel');
	dateLabelElement.innerHTML = Date();
}

main();
