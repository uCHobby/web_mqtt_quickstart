# Web Application MQTT Quick Start #

### Step by step guide for web based application development with MQTT.  
This guide is part 2 of a quick start series originally created for the [Austin IoT Hardware Focus Group](https://www.meetup.com/AustinIoT).

### Part 1
* ESP8266 MQTT Quick Start. How to get setup for IoT with MQTT and the ESP8266.
* Guide and all code examples hosted on [BitBucket here.](https://bitbucket.org/uCHobby/esp8266_mqtt_quickstart)
* Presented at the [Austin IoT Hardware Focus Meetup in July of 2016](http://www.meetup.com/AustinIoT/events/232515797/).

### Part 2 
* Web Application MQTT Quick Start. How to get setup to do IoT web applications using MQTT.
* Guide and all code examples hosted on [BitBucket here.](https://bitbucket.org/uCHobby/web_mqtt_quickstart)
* Scheduled for the [Austin IoT Hardware Focus Meetup in August of 2016](https://www.meetup.com/AustinIoT/events/232959066/).

#### What you need
* Git CLI
* Simple web server
* MQTT JavaScript client library
* An MQTT Broker to work with. (Austin IoT group has one for members)
* Code Editor ([Microsoft Visual Studio Code](https://code.visualstudio.com/c?utm_expid=101350005-24.YOq70TI1QcW9kAbMwmhePg.2&utm_referrer=https%3A%2F%2Fwww.google.com%2F))

### Who can I talk to about this?
Please contact [David Fowler, AKA uCHobby](http://fowlers.net/index.php/david) or the [Austin IoT group](http://www.meetup.com/AustinIoT/events/232515797/) for more information.
Reference this project, "ESP8266 MQTT Quick Start" in related communications.

### What can I use this code for? ###
You can do anything you like with the code presented here. If you find it useful, mention the original source or 
the Austin IoT group. We also hope for feedback on any code issues or improvements.

## Guide structure ##
This file is the guide. Source files are grouped by step number in sub-directories.  Step1, Step2, etc...  
Open the code for each step as you follow along with the explanations here, run that code and verify that things work.
Then move on.

The details of how the code functions are provided as comments in the code module and the guided walk through at the 
presentation.

# Step1: Working directory
Create a working directory by cloning the Bitbucket repo. This will be where we store all the files related to
our web application work. 

[_] Install Git which you can find [here](https://git-scm.com/)

[_] Open your command prompt and move to a directory where you store projects. 

[_] Use the following GIT clone command to create a quick start directory under your projects directory.

    `git clone https://bitbucket.org/uCHobby/web_mqtt_quickstart`

### Confirm that you have the following ready before continuing to Step 2
* Cloned the GIT repo onto your system. You should see a web_mqtt_quickstart directory under your projects directory with several sub-directories named step1, step2, etc...
* Code Editor 

### Walkthrough
* Project directory
* Git
* Bitbucket Repo
* .gitignore

# Step2: Simple Web Server
To do web application development we need to have a local web server.

[_] Install Node.js and lite-server using the [instructions here](https://blogs.msdn.microsoft.com/cdndevs/2016/01/24/visual-studio-code-and-local-web-server/) 

### Walkthrough
* Node.js
* NPM
* package.json
* lite-server

If you already have a web server and know how to setup it's source directory, point it at the new project directory. I recommend that 
you follow the instruction to get Node.js and lite-server.  This guide assumes you have, but for this guide we do not use any special 
server side features so any server should work fine.

# Step3: First Web Page index.html

Finally we are doing something! A basic hello world file is provided in the Step3 directory we are going to serve this page
up and do a quick edit to verify that we have a working development environment.

[_] Start the web server.

    `npm start`

-Note that you may get a fire-wall message when you start the server, enable all network access for your server.

[_] Open index.html in your browser [Step3/index.html](http://localhost:3000/Step3/index.html)

### Walkthrough
* index.html
* port 3000
* Node.js
* node_modules directory
* NPM
* package.json
* lite-server 
    -Note the directory is Step3 and the port.
    -Lite-server has a UI on port 3001. You can check it out [here](http://localhost:3001/)

### Troubleshooting
If your web page fails to load, it's likely that your server program is not set to deliver pages from the 
project directory.

You can walk back though the steps above and watch for any errors from npm when you do the install or the start
commands.

# Step4: Editing index.html

At this point we should have a working development environment for web application development! Lets prove it by 
editing the index.html file and seeing it change in the browser.

[_]Open index.html in your browser [Step4/index.html](http://localhost:3000/Step4/index.html)

[_]Open Step4/index.html in your editor. If your using VS Code you will see this file in the folder pane on left.

[_]Edit the title and body sections. Just change it up a bit, don't need to be fancy here. Don't forget to save.

[_]Confirm your changes in the browser. If your using lite-server this will happen automatically. If not use refresh.

### Walkthrough
* index.html
* lite-server 

### Troubleshooting
Your server may not be setup correctly, back up a step.  
You might be working in the wrong directory, we are in Step4.

# Step5: Web Code first steps.

Now we will add some files to our project. These include a JavaScript application, and some other files that help
with rendering a web page.  These files are included in the step5 directory so move there...

[_]Open Step5/index.html in your editor.

[_]Click the button to verify that JavaScript works for you.

[_]Open index.html in your browser [Step5/index.html](http://localhost:3000/Step5/index.html)

### Walkthrough
* index.html
* main.css
* main.js
* main()

### Troubleshooting
Nothing complicated changed here. Check that all your files are present and that you are working in the Step5 directory

# Step6: Add MQTT Library.

Now we will add the MQTT library to our project and connect to MQTT.
These files are included in the step6 directory so move there...

[_]Open Step6/index.html in your editor.

[_]Open main.js in your editor

[_]Edit the variable mqttAddress to point at your MQTT broker (line 10)

[_]Edit the variable mqttClientID to something that identifies you (line 11)

[_]Edit the variable mqttMyTopicPrefix to something unique for your project (line 22)

[_]Open index.html in your browser [Step6/index.html](http://localhost:3000/Step6/index.html)

[_]Open the debug console and the JavaScript console. Verify no errors and that you see a message from the 
    MQTT broker

### Troubleshooting
You can try using the "#" wildcard to get all messages and watch for yours.  You could also use the web client
tool mentioned in the appendix to help troubleshoot. If all else fails, try the Austin IoT MQTT slack channel.

### Walkthrough
* eclipse [Paho JavaScript library](https://eclipse.org/paho/clients/js/) for MQTT
* CDN Content Delivery Network
* 'use strict'
* console.log()
* Chrome Debugger
* topic & destinationName
* Wildcard #

# Step7: ESP8266 Button and LED.
Program your ESP8266 with some test code and test.
These files are included in the step7 directory so move there...

[_]Build your test circuit
    Assuming you use a NodeMCU for this project you will only need to power it up. The blue LED and flash button
    work fine for the supplied code. Most modules have this same arrangement of button and LED but you may need to
    connect a button to GPIO0 and and LED to GPIO16. 

[_]Open Step7/Step7.ino in your editor or with the Arduino IDE

[_]Edit the MQTT and Wifi settings details near the top of the code. Use the same setting as you did for the web work

    [_]Set your mqttID. Must be unique for every connection

    [_]Set your Wifi SSID and Password

    [_]Set your mqttTopicPrefix.  This needs to be unique for your project

    [_]Edit the mqttServer to point at your MQTT broker

[_]Save, Compile, and FLASH your ESP8266.

[_]Verify that each time a button is pressed the LED changes state.

[_]Use the MQTT web client tool to verify messages sent on button presses.

[_]Use the MQTT web client tool to control LED state.

### Troubleshooting
If you have trouble here, you might go back to the ESP8266 quick start. You should be able to FLASH code into your
module using the IDE.

### Walkthrough
* Setup and Loop
* connectionUpdate function
* event based code - changes from prior QS.
* buttonCheck function
* MQTT messages
* Using String for Payloads

# Step8: Web Application to control your ESP8266.
Build a web application to control the LED and show status for your project.
These files are included in the step8 directory so move there...

[_]Open Step8/main.js in your editor    

[_]Edit the MQTT settings details near the top of the code.

    [_]Set your mqttID. Must be unique for every connection

    [_]Set your mqttTopicPrefix.  This needs to be unique for your project

    [_]Edit the mqttServer to point at your MQTT broker

[_]Open index.html in your browser [Step8/index.html](http://localhost:3000/Step8/index.html)

[_]Open the debug console and the JavaScript console. 

[_]Hardware button toggle web view and vice versa. 

### Walkthrough
* css tricks
* raw JavaScript vrs JQuery
* message processing 
* sending the LED message

# Step9: Add Analog to the ESP8266 .
Program your ESP8266 with code to add analog sensing to the button processing.
These files are included in the Step9 directory so move there...

[_]Build your test circuit
    Assuming you use a NodeMCU for this project you will need to add something to generate an analog voltage to monitor. 
    The ESP8266 has a single ADC input but can also measure it's own power supply pin. For my testing a 10K pot and a voltage divider are
    connected. The pot ends are connected between ground and 3.3V with the wiper passing though 2.2K with a 1K shunt to ground.
    The small ESP modules like the ESP-01 don't all have the ADC exposed. You might change the code to generate a random value for 
    testing if you cant use the ADC pin.  Of course the ADC data could be replaced with any kind of analog sensor you can interface.

[_]Open Step9/Step9.ino in your editor or with the Arduino IDE

[_]Edit the MQTT and Wifi settings details near the top of the code. Use the same setting as you did for the web work

    [_]Set your mqttID. Must be unique for every connection

    [_]Set your Wifi SSID and Password

    [_]Set your mqttTopicPrefix.  This needs to be unique for your project

    [_]Edit the mqttServer to point at your MQTT broker

[_]Save, Compile, and FLASH your ESP8266.

[_]Verify that each time a button is pressed the LED changes state.

[_]Use the MQTT web client tool to verify messages sent on button presses.
    LED messages with on/off on button presses
    ADC messages when you turn the knob

[_]Use the MQTT web client tool to control LED state.
    LED messages when you click on the button
    The web page button matches the hardware

### Troubleshooting
A lot has to work at this point, but for this specific step everything should still be working, back up to a previous step if things fail here.

### Walkthrough
* New update function and timing control for an event
* Refactoring message handling
* ADC functions on the ESP

# StepA: Add a Gage to the web application.
Adding a Google Visualization Library Gage component to display analog data from the MQTT ADC message.
These files are included in the StepA directory so move there...

[_]Open StepA/main.js in your editor    

[_]Edit the MQTT settings details near the top of the code.

    [_]Set your mqttID. Must be unique for every connection

    [_]Set your mqttTopicPrefix.  This needs to be unique for your project

    [_]Edit mqttServer to point at your MQTT broker

[_]Open index.html in your browser [Step9/index.html](http://localhost:3000/Step8/index.html)

[_]Open the debug console and the JavaScript console. 

[_]Hardware button toggle web view and vice versa. 

[_]Play with the Pot and see the web gauge change. 

### Walkthrough
* changes to the html
* Goolge Visualization library
* Setup Gauge function
* Message parsing and events
* Update Gauge Data

# Appendix - Extra Information
#### Part 1
* Guide and all code examples hosted on [BitBucket here.](https://bitbucket.org/uCHobby/esp8266_mqtt_quickstart)
* Video of the [presentation](https://www.youtube.com/watch?v=t-i_zSuo59A)

#### HTML Links
 * [W3 Schools HTML Tutorial](http://www.w3schools.com/html/default.asp)
 * [W3 Schools CSS Tutorial](http://www.w3schools.com/html/html_css.asp)
 * [W3 Schools CSS Reference](http://www.w3schools.com/cssref/default.asp)
 * [W3 Schools JavaScript Tutorial](http://www.w3schools.com/js/default.asp)
 * [W3 Schools JavaScript Reference](http://www.w3schools.com/jsref/default.asp)
 
#### MQTT Links
 * [IBM Introduction](http://www.ibm.com/support/knowledgecenter/SSFKSJ_7.5.0/com.ibm.mm.tc.doc/tc00000_.htm)
 * [MQTT Reference](http://mqtt.org/)
 * [HiveMQ Online Tools List](http://www.hivemq.com/blog/seven-best-mqtt-client-tools)
 * [My Favorite Online MQTT Client](http://www.hivemq.com/demos/websocket-client/)
 * [Mosquitto MQTT Broker](http://mosquitto.org/) (This is what we use at Austin IoT)
 * [Paho Library Documentation](http://www.eclipse.org/paho/files/jsdoc/index.html)

