//AustionIoT ESP8266 MQTT Quickstart Step 4: MQTT Hello Chat
//https://bitbucket.org/uCHobby/esp8266_mqtt_quickstart

#include <ESP8266WiFi.h>    //Library for the ESP to work with Arduino 
#include <PubSubClient.h>   //Library for MQTT connections

void connectionUpdate(void);
void buttonCheck(void);
void onMQTTMessage(char* topic, byte* payload, unsigned int length);  //Callback function for MQTT Messages
void onNetConnect(void);
void onNetDisconnect(void);
void onMQTTConnect(void);
void onMQTTDisconnect(void);
void onButtonDown(void);
void onButtonUp(void);

const int BLUE_LED=16;    //Blue LED on the NodeMCU module
const int FLASH_BUTTON=0; //Flash button is connected to GPIO
const int LED_ON=0;		  //The LED is on when you write 0 to the GPIO
const int LED_OFF=1;	  //LED is off with 1.
const int BUTTON_ON=0;    //Button goes low on press
const int BUTTON_OFF=1;   //Button goes high normally

//Flags used to keep track of connection status
bool netConnected=false;		//Flag for network connection.  WiFi etc...
bool mqttConnected=false;		//Flag for MQTT connection.
bool buttonState=false;         //button state flag, pressed = true;
bool ledState=false;            //Flag to track status of the LED on or off

//**CHANGE THIS**
char* mqttID= "uCHobbyDevice";   	//ID used for MQTT connection, must be unique across everything, one per connection  **CHANGE THIS**
char* wifiSSID="iPhone Alfa";		//WiFi SSID.  **CHANGE THIS**
char* wifiPassword="011235813";	//Password for WiFi  **CHANGE THIS**
const char* mqttServer = "austiniot.com";	//Address for the MQTT broker **CHANGE THIS**
String mqttTopicPrefix = "atxiotmqttQS";

const int mqttPort=1883; 					//Port for MQTT connection

WiFiClient espClient;			//Adapter for network services used by PubSubClient

PubSubClient mqttClient( mqttServer,mqttPort,espClient);  //Adapter for MQTT Broker

void setup() {		//Arduino setup function, runs once at reset
	pinMode(BLUE_LED,OUTPUT);
	digitalWrite(BLUE_LED,LED_OFF);

	Serial.begin(115200);	//Set the Serial up for 115200 BPS
	delay(500);				//Wait a bit 
	Serial.println("\r\n\r\n");  //Print a few blank lines to get past junk
	Serial.println("ESP8266 MQTT Quick Start");  //Say hello over the terminal
	
	//Get connected to WiFi
	Serial.print("Connecting to ");
	Serial.println(wifiSSID);	
	WiFi.begin(wifiSSID, wifiPassword);	//Start up the WiFi connection
	mqttClient.setCallback(onMQTTMessage);	//Set the MQTT callback for when we get messages later.
}

void loop() {  		//Arduino loop function, runs over and over...
	connectionUpdate();  //Monitor the web and MQTT connections
	buttonCheck(); //Monitor the pushbutton
}

//Check the web and MQTT connection status
void connectionUpdate(void){
	//WiFi connection status
	if(WiFi.status()==WL_CONNECTED) {  			//Connected to WiFi?
		if(!netConnected){  					//New WiFi Connection?
			onNetConnect();
		}
		netConnected=true;						//Flag Connected
	}
	else { 										//Not connected to WiFi
		if(netConnected) { 						//Lost connection?
			onNetDisconnect();
		}
		netConnected=false;						//Flag Not Connected
	}

	//MQTT connection status
	if(mqttClient.connected()) {  				//Connected to MQTT
		if(!mqttConnected) {  					//New MQTT Connection?
			onMQTTConnect();
		}
		mqttConnected=true;						//Flag MQTT Connected
	}
	else {  									//Not connected to MQTT
		if (mqttConnected) {  					//Lost MQTT connection?
			onMQTTDisconnect();
		}
		mqttConnected=false;					//Flag MQTT not connected.
	}
	mqttClient.loop();							//Give the MQTT library some processing time CRITICAL!
}

void buttonCheck(void) { //Monitor the pushbutton
	int buttonValue;

	buttonValue=!digitalRead(FLASH_BUTTON); //use Not as button is low active. 0=active
	if (buttonValue!=buttonState) {  //Button Changed? 
		if (buttonValue) {  //if true then button is just now pressed
			onButtonDown();
		}
		else {
			onButtonUp();
		}
		buttonState=buttonValue;
	}
}

void onNetConnect(void){
	Serial.println("Connected WiFi!");
	Serial.println("MQTT: Connecting");
	mqttClient.connect(mqttID);			//Connect to MQTT broker now that we have a connection to the net.
}

void onNetDisconnect(void){
	Serial.println("Disconnected WiFi");
}

void onMQTTConnect(void){
	Serial.println("MQTT Connected");
	String rxTopic = mqttTopicPrefix+"/#";
	mqttClient.subscribe(rxTopic.c_str());	//Subscribe our messages
	String txTopic = mqttTopicPrefix+"/test";
	mqttClient.publish(txTopic.c_str(), "I'm Alive!");  //Send a hello.
}

void onMQTTDisconnect(void){
	Serial.println("MQTT Disconnected");
}

//Handle MQTT messages
void onMQTTMessage(char* topic, byte* payload, unsigned int length) {
	//Print message details
  	Serial.print("Message arrived [");
  	Serial.print(topic);
  	Serial.print("] ");

	String payloadString("");
	//The payload buffer might be binary so it's not really a character string and will not be 
	//automatically null terminated. We can't just stick a null on the end because we dont know
	//the size of the buffer and could end up overwriting something.  Instead, we walk though the
	//buffer from index zero to lenght and convert the bytes into a string a character at a time.
   	for (int i = 0; i < length; i++) { 
 	    payloadString+=(char)payload[i];
   	}
  	Serial.println(payloadString);

	String topicString(topic);
	if (payloadString=="on"){
		Serial.println("On Command");
		digitalWrite(BLUE_LED,LED_ON);
	} 
	if (payloadString=="off"){
		Serial.println("Off Command");
		digitalWrite(BLUE_LED,LED_OFF);
	} 
}

void onButtonDown(void){
	Serial.println("Down  ");

	String txTopic = mqttTopicPrefix+"/led";

	if(ledState) { //Is LED on now? 
		digitalWrite(BLUE_LED,LED_OFF);
		mqttClient.publish(txTopic.c_str(), "off");
		ledState=false;		
	}	
	else {  //LED is not on now.
		digitalWrite(BLUE_LED,LED_ON);
		mqttClient.publish(txTopic.c_str(), "on");
		ledState=true;		
	}
}

void onButtonUp(void){
	Serial.println("Up");	
}
